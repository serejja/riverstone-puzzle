Riverstone Puzzle
=================

[Prerequisites](#prerequisites)    
[Building](#building)    
[Running](#running)    
[Map description and restrictions](#map-description-and-restrictions)    
Prerequisites
-------------

1. Java (`1.8` was used during development, but should probably work fine with older versions)    
2. SBT (`0.13.8` was used during development)    

Building
--------

    # git clone https://gitlab.com/serejja/riverstone-puzzle.git
    # cd riverstone-puzzle
    # sbt assembly
    # mv target/scala-*/riverstone-puzzle-* riverstone-puzzle.jar
    
Running
-------

    # java -jar riverstone-puzzle.jar <map-file> <initial-velocity>
    
Parameter description:

1. `map-file` is the file name with the puzzle map definition. Please read further for details.    
2. `initial-velocity` must be a non-negative integer (0 allowed) and is the velocity at the start of the puzzle.

Example usage:

    # java -jar riverstone-puzzle.jar mapfile 2
    (0, 0)
    (3, 0)
    (7, 0)
    (10, 0)
    (12, 2)
    (15, 2)
    (15, 0)
    (18, 0)

    !--!---!--!----!--!
    --#---#----#---#-#-
    ---#----#---!--!---
    
The output here consists of 2 parts:

1. The list of (x, y) coordinates for each move performed to cross the river.
2. Resulting layout where actual steps are show as `!` characters.
 
Map description and restrictions
--------------------------------

The puzzle map is defined in a file and must meet several requirements:

1. Only `#` and `-` characters are allowed for tiles, where `#` means a stone which is walkable and `-` means water which is not walkable.
2. All lines must be of equal length. Variable line length file will be considered corrupted.
3. File contents may start/end with newline characters, they will be trimmed for convenience.
4. File may not be empty or contain empty lines with the exclusion described in #3.

Example map file contents:

```
#--#---#--#----#--#
--#---#----#---#-#-
---#----#---#--#---
```