lazy val root = (project in file(".")).
  settings(
    name := """riverstone-puzzle""",
    version := "1.0",
    scalaVersion := "2.11.7",
    mainClass in Compile := Some("com.gitlab.serejja.riverstone.Main")        
  )

libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % "test"

mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) =>
   {
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case x => MergeStrategy.first
   }
}