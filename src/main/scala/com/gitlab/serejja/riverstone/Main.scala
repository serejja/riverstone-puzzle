package com.gitlab.serejja.riverstone

import scala.io.Source
import scala.util.{Failure, Success, Try}

object Main {
  def main(args: Array[String]) {
    if (args.length != 2) {
      println("USAGE: riverstone-puzzle <map-file> <initial-velocity>")
      sys.exit(1)
    }

    val initialVelocity = Try(args(1).toInt) match {
      case Success(velocity) => velocity
      case Failure(ex) =>
        println("Initial velocity must be a number")
        sys.exit(1)
    }

    val rawMap = Try(Source.fromFile(args(0)).getLines().mkString("\n")) match {
      case Success(src) => src
      case Failure(ex) =>
        println(ex.getMessage)
        sys.exit(1)
    }

    val tileMap = TileMap(rawMap)
    val solution = Pathfinder(tileMap).find(initialVelocity)

    solution match {
      case Some(route) =>
        println(route.map { tile =>
          s"(${tile.x}, ${tile.y})"
        }.mkString("\n"))
        println
        println(tileMap.trace(route))
      case None => println("No solution found")
    }
  }
}
