package com.gitlab.serejja.riverstone

case class TileMap(tiles: Seq[Seq[Tile]]) {
  def startingTiles: Seq[Tile] = tiles.head.filter(_.walkable)

  def endingTiles: Seq[Tile] = tiles.last.filter(_.walkable)

  def neighbors(tile: Tile, velocity: Int): List[Tile] = {
    if (velocity <= 0) Nil
    else {
      val velocityRange = -velocity to velocity by velocity
      velocityRange.flatMap(x => velocityRange.flatMap { y =>
        if (x == 0 && y == 0) None
        else tiles.lift(tile.x + x).flatMap(_.lift(tile.y + y))
      }).toList
    }
  }

  def trace(route: List[Tile]): TileMap = {
    this.copy(route.foldLeft(tiles) { (tiles, tile) =>
      tiles.updated(tile.x, tiles(tile.x).updated(tile.y, tile.copy(marked = true)))
    })
  }

  override def toString: String = {
    tiles.transpose.map(row => row.map(tile => (tile.walkable, tile.marked) match {
      case (true, true) => "!"
      case (true, false) => "#"
      case _ => "-"
    }).mkString).mkString("\n")
  }
}

object TileMap {
  def apply(raw: String): TileMap = {
    val lines = raw.trim.split("\n")
    if (lines.exists(_.isEmpty)) throw new IllegalArgumentException("empty tile map row")
    if (lines.groupBy(_.length).size > 1)
      throw new IllegalArgumentException("lines of different length")

    new TileMap(lines.toList.zipWithIndex.map { case (line, y) =>
      line.zipWithIndex.map {
        case (tile, x) => Tile(tile, x, y)
      }
    }.transpose.map(_.toSeq))
  }
}

