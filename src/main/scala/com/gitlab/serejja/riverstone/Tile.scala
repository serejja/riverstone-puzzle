package com.gitlab.serejja.riverstone

case class Tile(x: Int, y: Int, walkable: Boolean = true,
                gCost: Int = 0, hCost: Int = 0, parent: Option[Tile] = None, marked: Boolean = false) {
  val fCost = gCost + hCost

  def trace(): List[Tile] = parent match {
    case None => List(this)
    case Some(parentTile) => parentTile.trace() ::: List(this)
  }

  override def hashCode(): Int = 31 * x + y

  override def equals(obj: Any): Boolean = obj match {
    case that: Tile => this.x == that.x && this.y == that.y
    case _ => false
  }

  override def toString: String = s"(${this.x}, ${this.y})"
}

object Tile {
  def apply(ch: Char, x: Int, y: Int): Tile = ch match {
    case '-' => Tile(x, y, walkable = false)
    case '#' => Tile(x, y, walkable = true)
    case _ => throw new IllegalArgumentException("invalid tile character")
  }
}