package com.gitlab.serejja.riverstone

import scala.collection.mutable

private case class Move(tile: Tile, velocity: Int)

case class Pathfinder(tileMap: TileMap) {
  def find(initialVelocity: Int): Option[List[Tile]] = {
    possiblePaths().foreach { case (start, end) =>
      val solution = find(start, end, initialVelocity)
      if (solution.isDefined)
        return solution
    }
    None
  }

  def find(origin: Tile, target: Tile, initialVelocity: Int): Option[List[Tile]] = {
    if (initialVelocity < 0) throw new IllegalArgumentException("initial velocity < 0")

    val toVisit = mutable.HashSet[Move]()
    val visited = mutable.HashSet[Move]()
    toVisit += Move(origin, initialVelocity)

    while (toVisit.nonEmpty) {
      val current = toVisit.minBy(move => (move.tile.fCost, move.tile.hCost, -move.velocity))

      toVisit -= current
      visited += current

      if (current.tile == target) {
        return Some(current.tile.trace())
      }

      velocityDeltas.foreach { delta =>
        tileMap.neighbors(current.tile, current.velocity + delta).foreach { neighborTile =>
          val neighbor = Move(neighborTile, current.velocity)
          if (neighborTile.walkable && !visited.contains(neighbor)) {
            val neighborCost = current.tile.gCost + Pathfinder.distance(current.tile, neighborTile)
            if (neighborCost < neighborTile.gCost || !toVisit.contains(neighbor)) {
              val nextTile = neighborTile.copy(gCost = neighborCost, hCost = Pathfinder.distance(neighborTile, target), parent = Some(current.tile))
              toVisit += Move(nextTile, current.velocity + delta)
            }
          }
        }
      }
    }

    None
  }

  private[riverstone] def possiblePaths(): Seq[(Tile, Tile)] = {
    for {
      start <- tileMap.startingTiles
      end <- tileMap.endingTiles
    } yield (start, end)
  }

  private def velocityDeltas = -1 to 1
}

object Pathfinder {
  def distance(a: Tile, b: Tile): Int = {
    val distanceX = Math.abs(a.x - b.x)
    val distanceY = Math.abs(a.y - b.y)

    if (distanceX > distanceY) 14 * distanceY + 10 * (distanceX - distanceY)
    else 14 * distanceX + 10 * (distanceY - distanceX)
  }
}
