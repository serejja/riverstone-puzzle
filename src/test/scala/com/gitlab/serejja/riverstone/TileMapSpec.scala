package com.gitlab.serejja.riverstone

import org.scalatest._

class TileMapSpec extends FlatSpec with Matchers {
  "TileMap" should "throw exception on empty input" in {
    val thrown = intercept[IllegalArgumentException](TileMap(""))
    assert(thrown.getMessage === "empty tile map row")
  }

  "TileMap" should "throw exception on variable length input" in {
    val rawMap =
      """###
        |--""".stripMargin
    val thrown = intercept[IllegalArgumentException](TileMap(rawMap))
    assert(thrown.getMessage === "lines of different length")
  }

  "TileMap" should "throw exception on illegal character" in {
    val rawMap =
      """foo
        |bar""".stripMargin
    val thrown = intercept[IllegalArgumentException](TileMap(rawMap))
    assert(thrown.getMessage === "invalid tile character")
  }

  "TileMap" should "be happy on valid input" in {
    val rawMap =
      """#-#
        |-#-
        |--#""".stripMargin
    noException should be thrownBy TileMap(rawMap)
  }

  "TileMap" should "find neighbors" in {
    assert(TileMap("###").neighbors(Tile(0, 0), -123) === Nil)
    assert(TileMap("###").neighbors(Tile(0, 0), 0) === Nil)
    assert(TileMap("###").neighbors(Tile(0, 0), 1) === List(Tile(1, 0)))
    assert(TileMap(
      """###
        |###""".stripMargin).neighbors(Tile(0, 0), 1) === List(Tile(0, 1), Tile(1, 0), Tile(1, 1)))
    assert(TileMap(
      """###
        |###""".stripMargin).neighbors(Tile(1, 0), 1) === List(Tile(0, 0), Tile(0, 1), Tile(1, 1), Tile(2, 0), Tile(2, 1)))
    assert(TileMap(
      """-###-
        |#####""".stripMargin).neighbors(Tile(2, 0), 2) === List(Tile(0, 0), Tile(4, 0)))
    assert(TileMap(
      """-###-
        |#####""".stripMargin).neighbors(Tile(2, 0), 0) === Nil)
    assert(TileMap(
      """-------
        |-------
        |-------
        |-------
        |-------
        |-------
        |-------""".stripMargin).neighbors(Tile(3, 3), 1)
      === List(Tile(2, 2), Tile(2, 3), Tile(2, 4), Tile(3, 2), Tile(3, 4), Tile(4, 2), Tile(4, 3), Tile(4, 4)))
    assert(TileMap(
      """-------
        |-------
        |-------
        |-------
        |-------
        |-------
        |-------""".stripMargin).neighbors(Tile(3, 3), 2)
      === List(Tile(1, 1), Tile(1, 3), Tile(1, 5), Tile(3, 1), Tile(3, 5), Tile(5, 1), Tile(5, 3), Tile(5, 5)))
    assert(TileMap(
      """-------
        |-------
        |-------
        |-------
        |-------
        |-------
        |-------""".stripMargin).neighbors(Tile(3, 3), 3)
      === List(Tile(0, 0), Tile(0, 3), Tile(0, 6), Tile(3, 0), Tile(3, 6), Tile(6, 0), Tile(6, 3), Tile(6, 6)))
  }

  "TileMap" should "trace results" in {
    assert(TileMap("###").trace(List(Tile(0, 0), Tile(2, 0))).toString === "!#!")
  }

  "TileMap" should "produce correct starting tiles" in {
    assert(TileMap(
      """#--
        |-#-
        |#-#""".stripMargin).startingTiles === List(Tile(0, 0), Tile(0, 2)))

    assert(TileMap(
      """#--
        |#--
        |#--""".stripMargin).startingTiles === List(Tile(0, 0), Tile(0, 1), Tile(0, 2)))

    assert(TileMap(
      """---
        |---
        |---""".stripMargin).startingTiles === Nil)
  }

  "TileMap" should "produce correct ending tiles" in {
    assert(TileMap(
      """#--
        |-#-
        |#-#""".stripMargin).endingTiles === List(Tile(2, 2)))

    assert(TileMap(
      """--#
        |--#
        |--#""".stripMargin).endingTiles === List(Tile(2, 0), Tile(2, 1), Tile(2, 2)))

    assert(TileMap(
      """---
        |---
        |---""".stripMargin).endingTiles === Nil)
  }
}
