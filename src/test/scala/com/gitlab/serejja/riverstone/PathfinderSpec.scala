package com.gitlab.serejja.riverstone

import org.scalatest._

class PathfinderSpec extends FlatSpec with Matchers {
  "Pathfinder" should "calculate distance between tiles" in {
    assert(Pathfinder.distance(Tile(0, 0), Tile(3, 0)) === 30)
    assert(Pathfinder.distance(Tile(0, 0), Tile(0, 3)) === 30)
    assert(Pathfinder.distance(Tile(5, 3), Tile(1, 3)) === 40)
    assert(Pathfinder.distance(Tile(0, 0), Tile(4, 4)) === 56)
    assert(Pathfinder.distance(Tile(1, 7), Tile(2, 2)) === 54)
  }

  "Pathfinder" should "find a simple solution" in {
    val tileMap = TileMap("###")
    val pathOpt = Pathfinder(tileMap).find(Tile(0, 0), Tile(2, 0), 0)

    assert(pathOpt !== None)
    assert(pathOpt.get === List(Tile(0, 0), Tile(1, 0), Tile(2, 0)))
  }

  "Pathfinder" should "jump over one gap" in {
    val tileMap = TileMap("#-#-#")
    val pathOpt = Pathfinder(tileMap).find(Tile(0, 0), Tile(4, 0), 1)

    assert(pathOpt !== None)
    assert(pathOpt.get === List(Tile(0, 0), Tile(2, 0), Tile(4, 0)))
  }

  "Pathfinder" should "jump over gap with initial velocity of 2" in {
    val tileMap = TileMap("#-#-#")
    val pathOpt = Pathfinder(tileMap).find(Tile(0, 0), Tile(4, 0), 2)

    assert(pathOpt !== None)
    assert(pathOpt.get === List(Tile(0, 0), Tile(2, 0), Tile(4, 0)))
  }

  "Pathfinder" should "know how to speed up" in {
    val tileMap = TileMap("#-#--#---#")
    val pathOpt = Pathfinder(tileMap).find(Tile(0, 0), Tile(9, 0), 1)

    assert(pathOpt !== None)
    assert(pathOpt.get === List(Tile(0, 0), Tile(2, 0), Tile(5, 0), Tile(9, 0)))
  }

  "Pathfinder" should "know how to speed down" in {
    val tileMap = TileMap("#---#--#--#-#")
    val pathOpt = Pathfinder(tileMap).find(Tile(0, 0), Tile(12, 0), 3)

    assert(pathOpt !== None)
    assert(pathOpt.get === List(Tile(0, 0), Tile(4, 0), Tile(7, 0), Tile(10, 0), Tile(12, 0)))
  }

  "Pathfinder" should "know how to move up and down" in {
    val rawMap =
      """#---#
        |-#-#-""".stripMargin
    val tileMap = TileMap(rawMap)
    val pathOpt = Pathfinder(tileMap).find(Tile(0, 0), Tile(4, 0), 1)

    assert(pathOpt !== None)
    assert(pathOpt.get === List(Tile(0, 0), Tile(1, 1), Tile(3, 1), Tile(4, 0)))
  }

  "Pathfinder" should "know how to move up and down changing velocity" in {
    val rawMap =
      """----------#---#
        |-#-#-----------
        |#--------------
        |---------------
        |------#--------""".stripMargin
    val tileMap = TileMap(rawMap)
    val pathOpt = Pathfinder(tileMap).find(Tile(0, 2), Tile(14, 0), 1)

    assert(pathOpt !== None)
    assert(pathOpt.get === List(Tile(0, 2), Tile(1, 1), Tile(3, 1), Tile(6, 4), Tile(10, 0), Tile(14, 0)))
  }

  "Pathfinder" should "solve the example case" in {
    val rawMap =
      """#--#---#--#----#--#
        |--#---#----#---#-#-
        |---#----#---#--#---""".stripMargin
    val tileMap = TileMap(rawMap)
    val pathOpt = Pathfinder(tileMap).find(Tile(0, 0), Tile(18, 0), 2)

    assert(pathOpt !== None)
    assert(pathOpt.get === List(Tile(0, 0), Tile(3, 0), Tile(7, 0), Tile(10, 0), Tile(12, 2), Tile(15, 2), Tile(15, 0), Tile(18, 0)))
  }

  "Pathfinder" should "fail to find solution on impossible case" in {
    val tileMap = TileMap("#---------#")
    val pathOpt = Pathfinder(tileMap).find(Tile(0, 0), Tile(10, 0), 1)

    assert(pathOpt === None)
  }

  "Pathfinder" should "throw exception on negative initial velocity" in {
    val thrown = intercept[IllegalArgumentException](Pathfinder(TileMap("#---------#")).find(Tile(0, 0), Tile(10, 0), -1))
    assert(thrown.getMessage === "initial velocity < 0")
  }

  "Pathfinder" should "find possible paths" in {
    assert(Pathfinder(TileMap("#---------#")).possiblePaths() === Seq((Tile(0, 0), Tile(10, 0))))
    assert(Pathfinder(TileMap("----------#")).possiblePaths() === Nil)
    assert(Pathfinder(TileMap("#----------")).possiblePaths() === Nil)
    assert(Pathfinder(TileMap(
      """#-#
        |#--""".stripMargin)).possiblePaths() === Seq((Tile(0, 0), Tile(2, 0)), (Tile(0, 1), Tile(2, 0))))

    assert(Pathfinder(TileMap(
      """#-#
        |--#""".stripMargin)).possiblePaths() === Seq((Tile(0, 0), Tile(2, 0)), (Tile(0, 0), Tile(2, 1))))

    assert(Pathfinder(TileMap(
      """#-#
        |#-#""".stripMargin)).possiblePaths() === Seq((Tile(0, 0), Tile(2, 0)), (Tile(0, 0), Tile(2, 1)),
      (Tile(0, 1), Tile(2, 0)), (Tile(0, 1), Tile(2, 1))))
  }

  "Pathfinder" should "find any possible solition without provided start-end points" in {
    val rawMap =
      """#--#---#--#----#--#
        |--#---#----#---#-#-
        |---#----#---#--#---""".stripMargin
    val tileMap = TileMap(rawMap)
    val pathOpt = Pathfinder(tileMap).find(2)

    assert(pathOpt !== None)
    assert(pathOpt.get === List(Tile(0, 0), Tile(3, 0), Tile(7, 0), Tile(10, 0), Tile(12, 2), Tile(15, 2), Tile(15, 0), Tile(18, 0)))
  }
}